﻿using System.Collections.Generic;
using UnityEngine;

public class Hole : MonoBehaviour
{
	// how high over the coordinate system origin the player is
	// TODO: should be automatic or aquired from config
	public float YTrapOffset = 1f;

	private int laneNumber = Temple.MaxLane - Temple.MinLane + 1;

    void Start()
    {
		// get lane indexes (min [1], max [iterations])
		// floor under those lanes will be removed
		int iterations = laneNumber - 1;
		HashSet<string> holeSet = new HashSet<string>();
		for(int i = 0; i < iterations; ++i) {
			int index = Random.Range(1, laneNumber);
			holeSet.Add(string.Format("Floor_{0}", index));
		}

		// get floor cubes to remove
		Transform trap = null;
		HashSet<Transform> toDestroy = new HashSet<Transform>();
		foreach(Transform t in GetComponentsInChildren<Transform>()) {
			if(holeSet.Contains(t.gameObject.name))
				toDestroy.Add(t);
			if(t.gameObject.name == "Trap")
				trap = t;
		}

		// set up trap triggers, remove floor cubes
		Quaternion rotation = new Quaternion(0f, 0f, 0f, 1f);
		foreach(Transform t in toDestroy) {

			Vector3 pos = t.position;
			pos.y += YTrapOffset;

			Instantiate(trap.gameObject, pos, rotation, GetComponent<Transform>());
			Destroy(t.gameObject);
		}

		// cleanup
		Destroy(trap.gameObject);
    }
}
