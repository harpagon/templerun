﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
	private static int colleNumber = 0; // counts collectibles

	public KeyCode MoveLeftKey, MoveRigtKey, LookBackKey;
	public int CurrLane = 0;
	public float Speed = 4f, SideSpeed = 4f;

	private enum State { // player movement state
		dead, moveLeft, forward, moveRight
	}

	private GameObject uiCanvas;
	private Text colleText;

	private float currHeight; // current Height
	private Vector3 velocity; // current player velocity
	private Quaternion rotation = new Quaternion(0f, 1f, 0f, 0f); // 180deg rotation around y
	private State state = State.forward;

	void Start()
	{
		velocity = new Vector3(0f, 0f, Speed);

		colleText = GetComponentInChildren<Text>();
		uiCanvas = colleText.gameObject.transform.parent.gameObject; // lel. Still better than foreach

		currHeight = transform.position.y;
	}

	void Update()
	{
		//
		if(state == State.dead)
			return;

		// if MoveLeft key pushed
		if(Input.GetKeyDown(MoveLeftKey) && CurrLane > Temple.MinLane) {
			CurrLane -= 1;
			state = State.moveLeft;
			velocity.x = -SideSpeed;
		}

		// if MoveRight key pushed
		if(Input.GetKeyDown(MoveRigtKey) && CurrLane < Temple.MaxLane) {
			CurrLane += 1;
			state = State.moveRight;
			velocity.x = SideSpeed;
		}

		// if LookBack key pushed
		if(Input.GetKeyDown(LookBackKey))
			transform.rotation *= rotation;

		// if reached current line (may occure during moveLeft or moveRight state
		float currLanePosX = CurrLane * Temple.LaneSize;
		bool ifCrossedLane = (state == State.moveLeft && transform.position.x < currLanePosX) ||
			(state == State.moveRight && transform.position.x > currLanePosX);
		if(ifCrossedLane) {
			Vector3 pos = transform.position;
			pos.x = currLanePosX;
			transform.position = pos;

			velocity.x = 0f;
			state = State.forward;
		}

		// if reached current height // needed for ramps
		if(transform.position.y > currHeight) {
			Vector3 pos = transform.position;
			pos.y = currHeight;
			transform.position = pos;

			velocity.y = 0f;
		}

		// finally, move
		transform.position += Time.deltaTime * velocity;
	}

	private void OnTriggerEnter(Collider collider) {
		if(collider.tag == "Collectible") {
			colleNumber += 1;
			colleText.text = string.Format("Collected: {0}", colleNumber);
			Destroy(collider.gameObject);
		}
		if(collider.tag == "Trap") {
			velocity.y = 0f;
			velocity.z = 0f;
			state = State.dead;
			createGameOverText();
		}
		if(collider.tag == "Ramp") {
			Ramp ramp = collider.transform.parent.gameObject.GetComponent<Ramp>();
			velocity.y = velocity.z * ramp.Height / ramp.Length;
			currHeight += ramp.Height;
		}
		if(collider.tag == "LevelEnd") {
			Portal portal = collider.GetComponentInParent<Portal>();
			SceneManager.LoadScene(portal.NextLevel);
		}
	}

	private void createGameOverText() {
		Font arial = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");

		GameObject go = new GameObject();
		go.transform.parent = uiCanvas.transform;

		Text t = go.AddComponent<Text>();
		t.text = "Game over!";
		t.font = arial;
		t.fontSize = 48;
		t.alignment = TextAnchor.MiddleCenter;
		t.color = new Color(1f, 0f, 0f, 1f);

		RectTransform rt = t.GetComponent<RectTransform>();
		rt.localPosition = new Vector3(0f, 0f, 0f);
		rt.sizeDelta = new Vector2(500f, 500f);
	}
}
