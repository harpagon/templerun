﻿using UnityEngine;

public class CollectibleCapsule : MonoBehaviour
{
	public float rotationspeed = 3f;

	void Update() {
		transform.Rotate(rotationspeed, 0, 0);
	}
}
