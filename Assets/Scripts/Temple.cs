﻿using System.Collections.Generic;
using UnityEngine;

public class Temple : MonoBehaviour
{
	// GAME CONFIG

	public static int MinLane = -2, MaxLane = 2;
	public static float LaneSize = 0.9f;

	// FOR GENERATED TEMPLES

	//   each prefab's length
	public Dictionary<GameObject, float> Lengths = new Dictionary<GameObject, float>();

	private Quaternion currOrien = new Quaternion(0f, 0f, 0f, 1f);
	private Vector3 currPos = new Vector3(0f, 0f, 0f);
	private GameObject currGO;

	private GameObject
		collectible,
		portal;

	private GameObject[]
		halls = new GameObject[4],
		traps = new GameObject[3];

	void Start() {
		loadResources();

		foreach(Transform t in transform)
			if(t.gameObject.name == "Portal")
				portal = t.gameObject;

		genLevel();
	}

	// yeah, this function is ugly
	// in fact, the whole project kinda is
	private void loadResources() {

		// load ...

		//collectible = Resources.Load<GameObject>("prefabs/Collectible"); // not used now

		halls[0] = Resources.Load<GameObject>("prefabs/Hall_1");
		halls[1] = Resources.Load<GameObject>("prefabs/Hall_2");
		halls[2] = Resources.Load<GameObject>("prefabs/Hall_3");
		halls[3] = Resources.Load<GameObject>("prefabs/Hall_5");

		traps[0] = Resources.Load<GameObject>("prefabs/Trap_Hole");
		traps[1] = Resources.Load<GameObject>("prefabs/Trap_Ramp");
		traps[2] = Resources.Load<GameObject>("prefabs/Trap_OpenRamp");

		// set lengths...
		// seriously, this can be done better
		// + I hate special treatment

		foreach(GameObject hall in halls)
			foreach(Transform t in hall.GetComponentsInChildren<Transform>())
				if(t.gameObject.name == "Floor")
					Lengths.Add(hall, t.localScale.z);

		Lengths.Add(traps[0], 2f);

		foreach(Transform t in traps[1].GetComponentsInChildren<Transform>())
			if(t.gameObject.name == "FloorExit")
				Lengths.Add(traps[1], t.position.z);

		foreach(Transform t in traps[2].GetComponentsInChildren<Transform>())
			if(t.gameObject.name == "FloorExit")
				Lengths.Add(traps[2], t.position.z);
	}

	// generate Level
	private void genLevel() {
		gen(halls[3]);
		for(int i = 0; i < 3; ++i) {
			gen(getRandGO(traps));
			gen(getRandGO(halls));
		}
		portal.transform.position = currPos;
	}

	// instantiate prefab, set its position
	private void gen(GameObject go) {
		Instantiate(go, currPos, currOrien, transform);
		currPos.z += Lengths[go];

		// unfortunetally, special treatment required
		// for now
		// and forever probably
		foreach(Transform t in go.GetComponentsInChildren<Transform>())
			if(t.gameObject.name == "FloorExit")
				currPos.y += t.position.y;
	}

	// get random GameObject from GameObject[]
	private GameObject getRandGO(GameObject[] gos) {
		return gos[Random.Range(0, gos.Length)];
	}
}
