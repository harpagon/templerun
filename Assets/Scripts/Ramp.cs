﻿using UnityEngine;

public class Ramp : MonoBehaviour
{
	public float Height { get; private set; }
	public float Length { get; private set; }
	public float FloorLength { get; private set; }

	void Start() {
		Vector3 tmp, pos = transform.position;
		Transform ramp = null, exit = null;
		Transform[] traps = new Transform[2];

		/* -----------------------------
		 * TODO: consider removing Cube (MeshFilter) Component Traps
		 * TODO: consider removing Mesh Renderer Component from Traps
		 * (I left them cuz they look kinda cool)
		 *
		 *     |exit|
		 *          |\
		 *          |   \ FloorLength
		 *   Height |       \ |ramp|
		 *     y    |           \
		 *          |______________\
		 *               Length     | <enter> |
		 *                 z
		 *
		 * tmp - just a placeholder for vectors that are being operated on.
		 * pos - Ramp position in the Temple
		 *
		 * traps - allows us detect if player misses (or slides off) the ramp.
		    ----------------------------- */

		foreach(Transform t in GetComponentsInChildren<Transform>()) {
			if(t.name == "FloorExit")
				exit = t;
			if(t.name == "FloorRamp")
				ramp = t;
			if(t.name == "Trap_L")
				traps[0] = t;
			if(t.name == "Trap_R")
				traps[1] = t;
		}

		// get Height & Length, adjust exit.position
		tmp = exit.position - pos;
		Height = tmp.y;
		Length = tmp.z;
		FloorLength = Mathf.Sqrt(Height * Height + Length * Length);

		// adjust position...
		tmp = ramp.position;
		tmp.x = Random.Range(Temple.MinLane, Temple.MaxLane) * Temple.LaneSize;
		//   ... ramp
		tmp.y += Height / 2f;
		tmp.z += Length / 2f;
		ramp.position = tmp;

		// adjust ramp...
		//   ... scale
		tmp = ramp.localScale;
		tmp.z = Length;
		ramp.localScale = tmp;
		//   ... orientation
		tmp.x = -Mathf.Asin(Height / Length) * 180 / Mathf.PI;
		tmp.y = 0f;
		tmp.z = 0f;
		ramp.Rotate(tmp);

		// adjust traps...
		//   ... position
		tmp = ramp.position;
		tmp.x -= (Temple.LaneSize / 2f) + (traps[0].localScale.x / 2f);
		tmp.y += 1f;
		traps[0].position = tmp;
		tmp.x += (Temple.LaneSize) + (traps[1].localScale.x);
		traps[1].position = tmp;
		//   ... scale
		tmp.x = traps[0].localScale.x;
		tmp.y = Height + 1f;
		tmp.z = exit.position.z - pos.z - 1f;
		traps[0].localScale = tmp;
		traps[1].localScale = tmp;
	}
}
